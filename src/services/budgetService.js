// fake API

const INCOME_MODE = { id: 1, name: 'Income' };
const EXPENSE_MODE = { id: 2, name: 'Expense' };

const data = [];

export const getAll = () => [...data];

export const getModes = () => [INCOME_MODE, EXPENSE_MODE];

export const add = (el) => {
  const newElement = { id: data.length, ...el };
  data.push(newElement);
  return newElement;
};

export const remove = (id) => {
  data.slice(
    data.findIndex((val) => val.id === id),
    1
  );
};

export default {
  getAll,
  getModes,
  add,
  remove,
};
