import budgetService from '../../services/budgetService';

const [INCOME_MODE, EXPENSE_MODE] = budgetService.getModes();

const state = {
  data: [],
};

const getters = {
  allData: ({ data }) => data,
  allIncomes: ({ data }) => data.filter((el) => el.mode === INCOME_MODE),
  allExpenses: ({ data }) => data.filter((el) => el.mode === EXPENSE_MODE),
};

const actions = {
  fetchBudgetData({ commit }) {
    const data = budgetService.getAll();
    commit('setData', data);
  },
  add({ commit }, record) {
    const newRecord = budgetService.add(record);
    commit('add', newRecord);
  },
  remove({ commit }, id) {
    budgetService.remove(id);
    commit('remove', id);
  },
};

const mutations = {
  setData: (state, data) => {
    state.data = data;
  },
  add: (state, newRecord) => {
    state.data.push(newRecord);
  },
  remove: (state, id) => {
    state.data = state.data.filter((val) => val.id !== id);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
