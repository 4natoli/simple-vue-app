import Vue from 'vue';
import VueRouter from 'vue-router';
import Add from '../components/Add';
import Incomes from '../components/Incomes';
import Expenses from '../components/Expenses';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Add',
    component: Add,
  },
  {
    path: '/incomes',
    name: 'Incomes',
    component: Incomes,
  },
  {
    path: '/expenses',
    name: 'Expenses',
    component: Expenses,
  },
];

export default new VueRouter({
  mode: 'history',
  routes,
});
